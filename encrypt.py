import sys
import os
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes
from binascii import b2a_hex

def encrypt_dir(path):
    try:
        for root, _, files in os.walk(path):
            for file in files:
                file_path = os.path.join(root, file)
                print(f"{file_path} is encrypting.")
                encrypt_file(file_path)
    except Exception as e:
        print(f"Error encrypting directory: {e}")

def encrypt_file(path):
    try:
        with open(path, 'rb') as f:
            plain_text = f.read()

        key = b'this is a 16 key'
        iv = get_random_bytes(AES.block_size)
        cipher = AES.new(key, AES.MODE_CFB, iv)
        ciphertext = iv + cipher.encrypt(plain_text)

        with open(path + ".bin", "wb") as file_out:
            file_out.write(ciphertext)
    except Exception as e:
        print(f"Error encrypting file {path}: {e}")

if name == "main":
    if len(sys.argv) < 2:
        print("Usage: python script.py <file or directory>")
        sys.exit(1)

    path = sys.argv[1]
    if os.path.exists(path):
        if os.path.isdir(path):
            encrypt_dir(path)
        elif os.path.isfile(path):
            encrypt_file(path)
        else:
            print("It's a special file (socket, FIFO, device file).")
    else:
        print("The specified path does not exist.")
