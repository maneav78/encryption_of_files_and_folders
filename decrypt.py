import sys
import os
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

def encrypt_dir(path):
    for root, _, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            print(f"{file_path} is encrypting.")
            encrypt_file(file_path)

def encrypt_file(path):
    with open(path, 'rb') as f:
        plain_text = f.read()

    key = b'this is a 16 key'

    iv = get_random_bytes(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)
    ciphertext = iv + cipher.encrypt(plain_text)

    with open(path + ".bin", "wb") as file_out:
        file_out.write(ciphertext)

def decrypt_file(encrypted_path, key):
    with open(encrypted_path, 'rb') as f:
        ciphertext = f.read()

    iv = ciphertext[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CFB, iv)
    plain_text = cipher.decrypt(ciphertext[AES.block_size:])

    decrypted_path = os.path.splitext(encrypted_path)[0]
    with open(decrypted_path, "wb") as file_out:
        file_out.write(plain_text)

if name == "main":
    if len(sys.argv) < 2:
        print("Usage: python script.py <file or directory>")
        sys.exit(1)

    path = sys.argv[1]
    if os.path.exists(path):
        if os.path.isdir(path):
            encrypt_dir(path)
        elif os.path.isfile(path):
            encrypt_file(path)
        else:
            print("It's a special file (socket, FIFO, device file).")
    else:
        print("The specified path does not exist.")
