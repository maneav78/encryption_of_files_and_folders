# File Encryption Script

This is a simple Python script for encrypting files and directories using AES encryption.

## Features

- Encrypt individual files.
- Encrypt entire directories recursively.
- Uses AES encryption with Cipher Feedback (CFB) mode.
- Supports Python 3.12.
- Including code for decrypting.

## Prerequisites

- Python 3.12
- Cryptodome library (`pip install pycryptodome`)

